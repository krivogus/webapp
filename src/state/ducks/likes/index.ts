import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { IPost, StateT, defaultState, ILikes } from 'src/types';
import { defaultReducers } from '../util';
import { normalize } from 'normalizr';
import { likesSchema } from 'src/state/normalizers';
import * as api from './api';
import { RootState } from 'src/state/store';

export type ILikesState = StateT<ILikes>;

const initialState: ILikesState = defaultState;

export const likePost = createAsyncThunk(
  'likes/create',
  async (payload: { postId: IPost['_id'] }) => api.likePost(payload.postId),
);

export const unlikePost = createAsyncThunk(
  'likes/remove',
  async (payload: { postId: IPost['_id'] }) => api.unlikePost(payload.postId),
);

export const likes = createSlice({
  name: 'likes',
  initialState,
  reducers: defaultReducers<ILikesState>(),
  extraReducers: builder => {
    builder.addCase(likePost.fulfilled, (state, { payload }) => {
      const { entities, result } = normalize(payload, likesSchema);
      state.byId[result] = entities.likes?.[result];
      state.all.unshift(result);
    });
    builder.addCase(unlikePost.fulfilled, (state, { payload }) => {
      const { entities, result } = normalize(payload, likesSchema);
      state.byId[result] = entities.likes?.[result];
    });
  },
});

export const likeById = (state: RootState) => state.likes.byId;
