export const getFullFileUrl = (filePath: string) =>
  `${process.env.REACT_APP_BASE_URL}/file-storage/${filePath}`;

export const config = {
  router: {
    invalidAuthRedirect: '/auth',
    validAuthRedirect: '/',
    home: '/',
    friends: '/',
  },
  auth: {
    storage: localStorage,
    tokenName: 'authToken',
    header: 'Authorization',
  },
  images: {
    profileBackground: getFullFileUrl('profile-background.webp'),
    defaultPostBackground: getFullFileUrl('profile-background.webp'),
  },
};
