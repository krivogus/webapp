import React, { MouseEvent } from 'react';
import {
  Button as MaterialButton,
  CircularProgress,
  Typography,
  Fade,
} from '@material-ui/core';
import { useStyles } from './index.style';

export interface ButtonProps {
  isFetching: boolean;
  text: string;
  type?: 'button' | 'submit' | 'reset';
  color?: 'primary' | 'secondary';
  onClick?: (e: MouseEvent) => void;
  [buttonProps: string]: any;
}

const Button = ({
  isFetching,
  text,
  type = 'submit',
  onClick,
  color = 'primary',
  ...rest
}: ButtonProps) => {
  const classes = useStyles();
  const spinnerColor: any = {
    primary: 'secondary',
    secondary: 'primary',
  }[color];

  return (
    <MaterialButton
      size="large"
      className={classes.button}
      type={type}
      variant="contained"
      onClick={onClick}
      color={color}
      {...rest}
      fullWidth
    >
      <Fade in={isFetching}>
        <CircularProgress
          color={spinnerColor}
          size={20}
          className={classes.positionAbsolute}
        />
      </Fade>
      <Fade in={!isFetching}>
        <Typography className={classes.positionAbsolute} variant="body1">
          {text}
        </Typography>
      </Fade>
    </MaterialButton>
  );
};

export default Button;
