import React, { ReactNode, FC } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { config } from 'src/config';

export interface IPrivateRouteProps extends RouteProps {
  isAuthenticated: boolean;
  children: ReactNode;
  [key: string]: any;
}

const PrivateRoute: FC<IPrivateRouteProps> = ({
  isAuthenticated,
  children,
  ...rest
}) => (
  <Route
    {...rest}
    render={({ location }) =>
      isAuthenticated ? (
        children
      ) : (
        <Redirect
          to={{
            pathname: config.router.invalidAuthRedirect,
            state: { from: location },
          }}
        />
      )
    }
  />
);

export default PrivateRoute;
