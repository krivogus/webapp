import React, { FC, ReactNode } from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { Switch, useLocation } from 'react-router-dom';
import { useStyles } from './routeAnimation.style';

interface IRouteAnimationProps {
  children: ReactNode;
}

const RouteAnimation: FC<IRouteAnimationProps> = ({ children }) => {
  const location = useLocation();
  const classes = useStyles({});

  return (
    <TransitionGroup>
      <CSSTransition key={location.key} classNames={classes} timeout={500}>
        <Switch location={location}>{children}</Switch>
      </CSSTransition>
    </TransitionGroup>
  );
};

export default RouteAnimation;
