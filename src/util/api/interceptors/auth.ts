import { config } from 'src/config';
import { getAuthToken, setAuthToken } from 'src/util/storage';

export const authRequestInterceptor = async (value: any) => {
  const authToken = getAuthToken();
  if (authToken) {
    value.headers[config.auth.header] = `Bearer ${authToken}`;
  }
  return await value;
};

export const authResponseInterceptor = async (value: any) => {
  const { token } = value.data;
  if (token) {
    setAuthToken(token);
  }
  return await value;
};
