import { DBProps } from './misc';

interface IAddress {
  city: string;
  country: string;
}

export interface IUser extends DBProps {
  profile: {
    avatarSrc?: string;
  };
  username: string | null;
  firstName?: string;
  lastName?: string;
  friends: string[];
  posts: string[];
  address?: IAddress;
  status?: string;
}
