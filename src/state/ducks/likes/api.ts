import { api } from 'src/util/api';
import { ILikes } from 'src/types';

export const likePost = async (postId: string) => {
  const { data } = await api.post<ILikes>(`/likes/${postId}`);
  return data;
};

export const unlikePost = async (postId: string) => {
  const { data } = await api.delete<ILikes>(`/likes/${postId}`);
  return data;
};
