import { PayloadAction } from '@reduxjs/toolkit';
import { StateT } from '../../types';

export interface IDefaultReducers<S extends Partial<StateT<any>>> {
  IRequest: (state: S, action: PayloadAction<undefined>) => void;
  ISuccess: (state: S, action: PayloadAction<Partial<S>>) => void;
  IError: (state: S, action: PayloadAction<string>) => void;
}

export const defaultReducers = <S extends Partial<StateT<any>>>() => {
  const request: IDefaultReducers<S>['IRequest'] = state => {
    state.isFetching = true;
  };
  const success: IDefaultReducers<S>['ISuccess'] = (state, { payload }) => {
    if (payload.all) state.all = payload.all;
    else
      state.all = [
        ...new Set([...(state.all ?? []), ...Object.keys(payload.byId ?? {})]),
      ];
    if (payload.byId) state.byId = { ...state.byId, ...payload.byId };
    if (payload.currentId) state.currentId = payload.currentId;
    if (payload.isFetching) state.isFetching = false;
    if (payload.error) state.error = null;
  };
  const error: IDefaultReducers<S>['IError'] = (state, { payload }) => {
    state.isFetching = false;
    state.error = payload;
  };

  return {
    request,
    success,
    error,
  };
};
