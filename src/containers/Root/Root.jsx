import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider as StoreProvider } from 'react-redux';
import { store } from '../../state';
import Router from '../../components/Router';
import { theme } from '../../util/theme';

const Root = () => (
  <StoreProvider store={store}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router />
    </ThemeProvider>
  </StoreProvider>
);

export default Root;
