import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  root: {
    fontWeight: 200,
    display: 'inline',
  },
  firstLetter: {
    fontWeight: 900,
    display: 'inline',
  },
});
