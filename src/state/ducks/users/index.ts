import {
  createSlice,
  PayloadAction,
  createSelector,
  createAsyncThunk,
} from '@reduxjs/toolkit';
import { IUser, StateT, defaultState } from 'src/types';
import { RootState } from 'src/state/store';
import * as api from './api';
import { defaultReducers } from '../util';
import { authUserId } from 'src/state/ducks/auth';

export interface IUsersState extends StateT<IUser> {}

const initialState: IUsersState = defaultState;

export const userById = (state: RootState) => state.users.byId;

export const currentUser = createSelector(
  [authUserId, userById],
  (authUserId, userById) => userById[String(authUserId)],
);

export const uploadAvatar = createAsyncThunk(
  'users/uploadAvatar',
  async (file: File, thunkApi) => {
    const state = thunkApi.getState() as RootState;
    const { _id } = currentUser(state) as IUser;

    const avatarSrc = await api.uploadAvatar(file);
    return { avatarSrc, userId: _id };
  },
);

export const users = createSlice({
  name: 'users',
  initialState,
  reducers: defaultReducers<IUsersState>(),
  extraReducers: builder => {
    builder.addCase(
      uploadAvatar.fulfilled,
      (
        state,
        { payload }: PayloadAction<{ userId: IUser['_id']; avatarSrc: string }>,
      ) => {
        state.byId[payload.userId].profile.avatarSrc = payload.avatarSrc;
      },
    );
  },
});
