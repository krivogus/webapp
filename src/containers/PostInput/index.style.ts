import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4),
      paddingLeft: theme.spacing(theme.overhang.medium),
      paddingRight: theme.spacing(theme.overhang.medium),
      [theme.breakpoints.down('sm')]: {
        paddingLeft: theme.spacing(theme.overhang.small),
        paddingRight: theme.spacing(theme.overhang.small),
      },
    },
    root: {
      padding: `${theme.spacing(4)}px ${theme.spacing(2)}px`,
    },
    inputContainer: {
      flexGrow: 1,
    },
    input: {
      width: '100%',
    },
  }),
);
