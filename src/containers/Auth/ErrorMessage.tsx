import React, { FC } from 'react';
import { Grid, Fade } from '@material-ui/core';
import { useStyles } from './index.style';
import { useField } from 'formik';
import { IFormValues } from '.';

export interface IErrorMessage<Values> {
  name: keyof Values;
}

const ErrorMessage: FC<IErrorMessage<IFormValues>> = props => {
  const classes = useStyles({});
  const [, meta] = useField(props);

  return (
    <Grid className={classes.error}>
      <Fade in={!!meta.touched && !!meta.error}>
        <Grid>{meta.error}</Grid>
      </Fade>
    </Grid>
  );
};

export default ErrorMessage;
