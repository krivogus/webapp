import { api } from 'src/util/api';
import { IPost } from 'src/types';

export const createPost = async (text: string, photo: File | null) => {
  const formData = new FormData();
  photo && formData.append('photo', photo);
  formData.append('text', text);
  const { data } = await api.post<IPost>('/post', formData);
  return data;
};
