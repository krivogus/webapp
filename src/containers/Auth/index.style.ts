import { Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: '100vh',
      transform: 'translateY(20vh)',
      position: 'fixed',
    },
    logo: {
      marginBottom: theme.spacing(5),
      textAlign: 'center',
    },
    requirements: {
      marginLeft: theme.spacing(1),
    },
    error: {
      height: theme.spacing(2),
    },
    authError: {
      margin: theme.spacing(2),
    },
  }),
);
