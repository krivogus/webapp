import React, { FC } from 'react';
import { TextField } from '@material-ui/core';
import { Field } from 'formik';

export interface ITextInput {
  name: string;
}

const TextInput: FC<ITextInput> = ({ name }) => (
  <Field
    name={name}
    as={TextField}
    margin="normal"
    variant="outlined"
    label={name}
    type={name}
    fullWidth
    autoComplete="off"
  />
);

export default TextInput;
