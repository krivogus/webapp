import React, { FC } from 'react';
import { IUser } from 'src/types';
import FriendListItem from './FriendListItem';
import { useStyles } from './index.style';
import { Grid, Paper } from '@material-ui/core';

export interface IFriendListProps {
  friends: IUser[];
}

const FriendList: FC<IFriendListProps> = ({ friends }) => {
  const classes = useStyles();

  return (
    <Grid className={classes.wrapper}>
      <Paper>
        {friends.map(friend => (
          <FriendListItem key={friend._id as string} friend={friend} />
        ))}
      </Paper>
    </Grid>
  );
};

export default FriendList;
