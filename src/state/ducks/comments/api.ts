import { api } from 'src/util/api';
import { IComment } from 'src/types';

export const createComment = async (text: string, postId: string) => {
  const { data } = await api.post<IComment>(`/comment/${postId}`, {
    data: { text },
  });
  return data;
};
