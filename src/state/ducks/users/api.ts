import { api } from 'src/util/api';

export const uploadAvatar = async (file: File): Promise<string> => {
  const formData = new FormData();
  formData.append('file', file);

  const {
    data: { fileId },
  } = await api.patch('/profile/avatar', formData);
  return fileId;
};
