import {
  createSlice,
  createAsyncThunk,
  createSelector,
} from '@reduxjs/toolkit';
import { IPost, StateT, defaultState, IComment } from 'src/types';
import { defaultReducers } from '../util';
import { normalize } from 'normalizr';
import { postSchema, commentSchema } from 'src/state/normalizers';
import * as api from './api';
import { RootState } from 'src/state/store';
import { likeById } from '../likes';
import { commentById, createComment } from '../comments';

export type IPostsState = StateT<IPost>;

const initialState: IPostsState = defaultState;

export const createPost = createAsyncThunk(
  'post/create',
  async (payload: { text: string; photo: File | null }) =>
    api.createPost(payload.text, payload.photo),
);

export const posts = createSlice({
  name: 'posts',
  initialState,
  reducers: defaultReducers<IPostsState>(),
  extraReducers: builder => {
    builder.addCase(createPost.fulfilled, (state, { payload }) => {
      const { entities, result } = normalize(payload, postSchema);
      state.byId[result] = entities.posts?.[result];
      state.all.unshift(result);
    });
    builder.addCase(createComment.fulfilled, (state, { payload }) => {
      const { entities, result } = normalize(payload, commentSchema);
      const comment = entities.comments?.[result] as IComment;
      if (!comment) return;
      (state.byId[String(comment.post)].comments as string[])?.push(result);
    });
  },
});

export const allPostIds = (state: RootState) => state.posts.all;
export const postById = (state: RootState) => state.posts.byId;

export const allPosts = createSelector(
  [allPostIds, postById, likeById, commentById],
  (allPostIds, postById, likeById, commentById) =>
    allPostIds.map(id => ({
      ...postById[id],
      likes: likeById[String(postById[id].likes)],
      comments: (postById[id].comments as string[])?.map(id => commentById[id]),
    })),
);
