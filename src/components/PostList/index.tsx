import React, { FC } from 'react';
import { IPost, ILikes } from 'src/types';
import { Grid } from '@material-ui/core';
import PostItem, { IPostItemProps } from './PostItem';

type IPostListProps = Omit<IPostItemProps, 'isLikedByMe' | 'post'> & {
  posts: IPost[];
};

const PostList: FC<IPostListProps> = ({ posts, ...itemProps }) => (
  <Grid container spacing={2} direction="column" wrap="nowrap">
    {posts.map(post => (
      <Grid item key={post._id as string}>
        <PostItem
          post={post}
          isLikedByMe={
            !!((post.likes as ILikes)?.users as string[])?.find(
              id => id === itemProps.me._id,
            )
          }
          {...itemProps}
        />
      </Grid>
    ))}
  </Grid>
);

export default PostList;
