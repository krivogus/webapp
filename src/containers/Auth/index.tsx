import React, { FC } from 'react';
import { Container, Grid, Typography, Fade } from '@material-ui/core';
import * as Yup from 'yup';
import { Formik, Form, FormikProps, FormikHelpers } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { authenticate, register, RootState } from '../../state';
import Button from '../../components/Button';
import { useStyles } from './index.style';
import Logo from '../../components/Logo';
import ErrorChecker from './ErrorChecker';
import CustomErrorMessage from './ErrorMessage';
import TextInput from './TextInput';

const passwordRules = {
  maxChars: 'Password must be less then 30 chars',
  minChars: 'Password must be more then 3 chars',
  capital: 'Password must contain CAPITAL chars',
  lowercase: 'Password must contain lowercase chars',
  digit: 'Password must contain at least 1 d1g1t',
};

const validationSchema = Yup.object({
  email: Yup.string()
    .email('Invalid email address')
    .required('Required'),
  password: Yup.string()
    .max(30, passwordRules.maxChars)
    .min(3, passwordRules.minChars)
    .matches(/[A-Z]/, passwordRules.capital)
    .matches(/[a-z]/, passwordRules.lowercase)
    .matches(/[0-9]/, passwordRules.digit)
    .required('Required'),
});

export interface IFormValues {
  email: string;
  password: string;
}

const Auth: FC = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const authError = useSelector(({ auth }: RootState) => auth.error);

  const signUp = (
    { email, password }: IFormValues,
    setSubmitting: FormikHelpers<IFormValues>['setSubmitting'],
  ) => async () => {
    await dispatch(register(email, password));
    setSubmitting(false);
  };

  const signIn = async (
    { email, password }: IFormValues,
    { setSubmitting }: FormikHelpers<IFormValues>,
  ) => {
    await dispatch(authenticate(email, password));
    setSubmitting(false);
  };

  const initialValues: IFormValues = { email: '', password: '' };
  return (
    <Grid container justify="center" className={classes.root}>
      <Container maxWidth="xs">
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={signIn}
        >
          {({
            isSubmitting,
            isValid,
            values,
            setSubmitting,
            touched,
          }: FormikProps<IFormValues>) => {
            const isDisabledButtons =
              isSubmitting || !isValid || !Object.keys(touched).length;
            const validationErrors: string[] = [];
            try {
              validationSchema.validateSync(values, {
                abortEarly: false,
              });
            } catch (error) {
              validationErrors.push(...error.errors);
            }
            return (
              <Form>
                <Grid item className={classes.logo}>
                  <Logo />
                </Grid>
                <TextInput name="email" />
                <CustomErrorMessage name="email" />
                <TextInput name="password" />
                <CustomErrorMessage name="password" />
                <Grid container direction="row" spacing={3}>
                  <Grid item xs>
                    <Button
                      variant="contained"
                      isFetching={isSubmitting}
                      text="Sign in"
                      type="submit"
                      disabled={isDisabledButtons}
                    />
                  </Grid>
                  <Grid item xs>
                    <Button
                      onClick={signUp(values, setSubmitting)}
                      variant="outlined"
                      isFetching={isSubmitting}
                      disabled={isDisabledButtons}
                      text="Sign up"
                      type="button"
                    />
                  </Grid>
                  <Grid container className={classes.authError}>
                    <Fade in={!!authError}>
                      <Grid>{authError}</Grid>
                    </Fade>
                  </Grid>
                  <Fade in={!isValid}>
                    <Grid item container direction="column">
                      <Grid item>
                        <Typography variant="body1">
                          Password requirements:
                        </Typography>
                      </Grid>
                      {Object.values(passwordRules).map(rule => (
                        <ErrorChecker
                          key={rule}
                          text={rule}
                          isChecked={
                            !validationErrors.find(error => error === rule)
                          }
                        />
                      ))}
                    </Grid>
                  </Fade>
                </Grid>
              </Form>
            );
          }}
        </Formik>
      </Container>
    </Grid>
  );
};

export default Auth;
