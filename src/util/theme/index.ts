import { createMuiTheme } from '@material-ui/core/styles';

interface ICustomThemeOptions {
  overhang: {
    small: number;
    medium: number;
    large: number;
  };
}
declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme extends ICustomThemeOptions {}
  interface ThemeOptions extends Partial<ICustomThemeOptions> {}
}

export const theme = createMuiTheme({
  overhang: {
    small: 2,
    medium: 5,
    large: 6,
  },
  palette: {
    primary: {
      main: '#3B4653',
    },
    secondary: {
      main: '#E5E5E5',
    },
  },
  typography: {
    fontFamily: [
      'Montserrat',
      'sans-serif',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
});
