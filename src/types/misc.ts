export interface DBProps {
  _id: string;
  createdAt?: string;
  updatedAt?: string;
}
