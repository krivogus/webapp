import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

const avatarLength = 25;
const topMargin = 8;

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      width: '100%',
    },
    root: {
      background: theme.palette.primary.main,
    },
    card: {
      background: theme.palette.primary.main,
      position: 'relative',
    },
    media: {
      height: 140,
    },
    contentRoot: {
      marginTop: theme.spacing((topMargin + avatarLength) / 2),
    },
    avatarImage: {
      minWidth: '100%',
      height: '100%',
    },
    imageContainer: {
      width: '100%',
      height: '100%',
    },
    profileImage: {
      borderRadius: theme.spacing(1) / 2,
      position: 'absolute',
      top: theme.spacing(topMargin),
      left: '50%',
      transform: 'translateX(-50%)',
      height: theme.spacing(avatarLength),
      width: theme.spacing(avatarLength),
      overflow: 'hidden',
    },
    profileImageInput: {
      width: '100%',
      height: '100%',
      border: `1px dashed ${theme.palette.primary.main}`,
    },
  }),
);
