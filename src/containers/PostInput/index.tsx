import React, { FC, ChangeEvent, useState, useRef } from 'react';
import { useStyles } from './index.style';
import { Paper, Grid, Avatar, InputBase } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import * as state from 'src/state';
import { getFullFileUrl } from 'src/config';
import IconButton from '@material-ui/core/IconButton';
import InsertPhotoIcon from '@material-ui/icons/InsertPhoto';
import InsertPhotoOutlinedIcon from '@material-ui/icons/InsertPhotoOutlined';
import SendIcon from '@material-ui/icons/Send';
import SendOutlinedIcon from '@material-ui/icons/SendOutlined';
import { createPost } from 'src/state';

const PostInput: FC = () => {
  const currentUser = useSelector(state.currentUser);
  const [file, setFile] = useState<File | null>(null);
  const [text, setText] = useState<string>('');
  const dispatch = useDispatch();
  const classes = useStyles({});
  const fileRef = useRef<HTMLInputElement>(null);
  const textRef = useRef<HTMLInputElement>(null);
  const onChangeText = (
    e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => {
    setText(e.currentTarget.value);
  };
  const onSubmit = async () => {
    const promise = dispatch(createPost({ text, photo: file }));
    setFile(null);
    setText('');
    (fileRef as any).current.value = null;
    await promise;
  };

  const onFile = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e.currentTarget.files) return;
    setFile(e.currentTarget.files[0]);
  };

  const canBeSent = text.match(/\w+/) || file;

  return (
    <form onSubmit={onSubmit}>
      <Grid container direction="column" className={classes.wrapper}>
        <Paper className={classes.root}>
          <Grid container spacing={2} wrap="nowrap">
            <Grid item>
              <Avatar
                src={
                  currentUser.profile?.avatarSrc &&
                  getFullFileUrl(currentUser.profile.avatarSrc)
                }
              />
            </Grid>
            <Grid
              item
              container
              direction="column"
              justify="center"
              className={classes.inputContainer}
            >
              <InputBase
                placeholder="What’s on your mind?"
                className={classes.input}
                value={text}
                onChange={onChangeText}
                multiline
                ref={textRef}
              />
            </Grid>
            <Grid item>
              <IconButton aria-label="insert" component="label">
                <input
                  type="file"
                  style={{ display: 'none' }}
                  accept="image/*"
                  onChange={onFile}
                  ref={fileRef}
                />
                {file ? <InsertPhotoIcon /> : <InsertPhotoOutlinedIcon />}
              </IconButton>
            </Grid>
            <Grid item>
              <IconButton
                aria-label="send"
                onClick={onSubmit}
                disabled={!canBeSent}
              >
                {canBeSent ? <SendIcon /> : <SendOutlinedIcon />}
              </IconButton>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </form>
  );
};

export default PostInput;
