import React, { FC } from 'react';
import { Typography } from '@material-ui/core';
import { useStyles } from './index.style';

const Logo: FC<{}> = () => {
  const { root, firstLetter } = useStyles();

  return (
    <div>
      <Typography className={firstLetter} variant="h4">
        Social
      </Typography>
      <Typography className={root} variant="h4">
        ity
      </Typography>
    </div>
  );
};

export default Logo;
