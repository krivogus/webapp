export interface StateT<Entity extends { _id: string | number }> {
  isFetching: boolean;
  error: string | null;
  byId: { [_id: string]: Entity };
  all: Entity['_id'][];
  currentId: Entity['_id'] | null;
}

export const defaultState = {
  isFetching: false,
  error: null,
  byId: {},
  all: [],
  currentId: null,
};
