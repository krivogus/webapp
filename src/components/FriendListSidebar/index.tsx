import React, { FC } from 'react';
import List from '@material-ui/core/List';
import { useStyles } from './index.style';
import FriendListItem from './FriendListItem';
import { IUser } from '../../types';
import ListItemText from '@material-ui/core/ListItemText';
import { Paper } from '@material-ui/core';
import ListItem from '@material-ui/core/ListItem';
import Grid from '@material-ui/core/Grid';

interface IFriendList {
  friends: IUser[];
}

const FriendList: FC<IFriendList> = ({ friends }) => {
  const classes = useStyles({});

  return (
    <Paper classes={{ root: classes.root }}>
      <List className={classes.list}>
        <ListItem>
          <Grid container justify="space-between">
            <ListItemText
              primaryTypographyProps={{ color: 'secondary' }}
              primary="FRIENDS"
            />
            <Grid item>
              <ListItemText
                primaryTypographyProps={{ color: 'secondary' }}
                primary={friends.length}
              />
            </Grid>
          </Grid>
        </ListItem>
        {friends.map(({ username }: IUser) => (
          <FriendListItem
            key={username as string}
            username={username as string}
            avatarSrc={username as string}
          />
        ))}
      </List>
    </Paper>
  );
};

export default FriendList;
