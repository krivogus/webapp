import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getMe, RootState } from '../../state';
import { useHistory } from 'react-router-dom';
import { config } from 'src/config';

const AuthProvider: FC<any> = ({ children }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { currentId } = useSelector(({ auth }: RootState) => auth);

  useEffect(() => {
    if (currentId) {
      history.replace(config.router.validAuthRedirect);
    } else {
      dispatch(getMe());
    }
    // eslint-disable-next-line
  }, [currentId]);

  return children;
};

export default AuthProvider;
