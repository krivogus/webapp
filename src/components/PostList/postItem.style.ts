import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      paddingLeft: theme.spacing(theme.overhang.medium),
      paddingRight: theme.spacing(theme.overhang.medium),
      [theme.breakpoints.down('sm')]: {
        paddingLeft: theme.spacing(theme.overhang.small),
        paddingRight: theme.spacing(theme.overhang.small),
      },
    },
    root: {
      overflow: 'visible',
    },
    media: {
      [theme.breakpoints.down('sm')]: {
        transform: `translateX(-${theme.spacing(theme.overhang.small)}px)`,
        width: `calc(100% + ${theme.spacing(theme.overhang.small) * 2}px)`,
      },
      transform: `translateX(-${theme.spacing(theme.overhang.medium)}px)`,
      borderRadius: theme.spacing(1),
      height: 0,
      width: `calc(100% + ${theme.spacing(theme.overhang.medium) * 2}px)`,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
    like: {
      transition: '.4s',
    },
    liked: {
      color: red[500],
    },
    statisticsTypography: {
      marginRight: theme.spacing(1),
    },
  }),
);
