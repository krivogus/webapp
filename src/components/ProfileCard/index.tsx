import React, { MouseEvent, forwardRef } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { useStyles } from './index.style';
import { config } from 'src/config';
import { Paper } from '@material-ui/core';
import { IUser } from 'src/types';

interface IProfileCardProps {
  uploadAvatar: (e: MouseEvent<HTMLLabelElement>) => void;
  avatarSrc?: string;
  username: IUser['username'];
}

const ProfileCard = forwardRef<HTMLInputElement, IProfileCardProps>(
  ({ uploadAvatar, avatarSrc, username }, ref) => {
    const classes = useStyles({});

    return (
      <Card classes={{ root: classes.card }}>
        <Grid container justify="center" className={classes.profileImage}>
          {avatarSrc ? (
            <Paper className={classes.imageContainer}>
              <img
                src={avatarSrc}
                alt="profile"
                className={classes.avatarImage}
              />
            </Paper>
          ) : (
            <Paper className={classes.imageContainer}>
              <Button
                component="label"
                onChange={uploadAvatar}
                className={classes.profileImageInput}
              >
                Upload <br />
                Profile <br />
                Image
                <input
                  type="file"
                  accept="image/*"
                  style={{ display: 'none' }}
                  ref={ref}
                />
              </Button>
            </Paper>
          )}
        </Grid>
        <CardMedia
          className={classes.media}
          image={config.images.profileBackground}
          title="Avatar"
        />
        <CardContent className={classes.contentRoot}>
          <Grid
            container
            item
            zeroMinWidth
            direction="column"
            alignItems="center"
          >
            <Typography
              gutterBottom
              color="secondary"
              variant="h5"
              component="h2"
              noWrap
              style={{ width: '100%', textAlign: 'center' }}
            >
              {username}
            </Typography>
            <Typography
              gutterBottom
              color="secondary"
              variant="body2"
              component="h2"
            >
              Current Location
            </Typography>
            <Typography
              gutterBottom
              color="secondary"
              variant="h5"
              component="h2"
            >
              About me
            </Typography>
            <Typography
              align="center"
              variant="body2"
              color="secondary"
              component="p"
            >
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora
              necessitatibus vero dolores aspernatur accusantium eaque autem,
            </Typography>
          </Grid>
        </CardContent>
      </Card>
    );
  },
);

export default ProfileCard;
