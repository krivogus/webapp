import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  enter: {
    opacity: 0,
    zIndex: 0,
  },
  exit: {
    opacity: 0,
    transition: 'opacity 250ms ease-in',
    zIndex: 0,
  },
  enterActive: {
    opacity: 1,
    zIndex: 1,
    transition: 'opacity 250ms ease-in',
  },
});
