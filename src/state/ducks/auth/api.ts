import { IUser } from 'src/types';
import { api } from 'src/util/api';

export const register = async (
  username: string,
  password: string,
): Promise<IUser> => {
  const {
    data: { user, token },
  } = await api.post('/auth/register', { username, password });
  return { ...user, token };
};

export const login = async (
  username: string,
  password: string,
): Promise<IUser> => {
  const {
    data: { user, token },
  } = await api.post('/auth/login', { username, password });
  return { ...user, token };
};

export const getUser = async (): Promise<IUser> => {
  const { data } = await api.get('/auth/user');
  return data.user;
};
