import { IComment } from './comment';
import { ILikes } from './likes';
import { IUser } from './user';
import { DBProps } from './misc';

export interface IPost extends DBProps {
  data: {
    text?: string;
    photo?: string;
  };
  author: IUser;
  comments?: IComment[] | string[];
  likes: ILikes | string;
}
