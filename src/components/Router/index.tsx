import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Auth from 'src/containers/Auth';
import Home from 'src/containers/Home';
import NotFound from 'src/containers/NotFound';
import PrivateRoute from './PrivateRoute';
import { useSelector } from 'react-redux';
import { RootState } from 'src/state';
import AuthProvider from '../../containers/AuthProvider';
import { config } from 'src/config';
import RouteAnimation from './RouteAnimation';
import Friends from 'src/containers/Friends';

const Router = () => {
  const auth = useSelector(({ auth }: RootState) => auth);

  return (
    <BrowserRouter>
      <AuthProvider>
        <RouteAnimation>
          <PrivateRoute
            exact
            isAuthenticated={!!auth.currentId}
            path={config.router.validAuthRedirect}
          >
            <Home />
          </PrivateRoute>
          <Route exact path={config.router.invalidAuthRedirect}>
            <Auth />
          </Route>
          <PrivateRoute
            exact
            isAuthenticated={!!auth.currentId}
            path="/friends"
          >
            <Friends />
          </PrivateRoute>
          <Route path="*">
            <NotFound />
          </Route>
        </RouteAnimation>
      </AuthProvider>
    </BrowserRouter>
  );
};

export default Router;
