import { config } from 'src/config';

const { storage, tokenName } = config.auth;

export const getAuthToken = (): string | null => {
  return storage.getItem(tokenName);
};

export const setAuthToken = (authToken: string) => {
  storage.setItem(tokenName, authToken);
};

export const removeAuthToken = () => {
  storage.removeItem(tokenName);
};
