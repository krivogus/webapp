import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { IPost, StateT, defaultState, IComment } from 'src/types';
import { defaultReducers } from '../util';
import { normalize } from 'normalizr';
import { commentSchema } from 'src/state/normalizers';
import * as api from './api';
import { RootState } from 'src/state/store';

export type ICommentsState = StateT<IComment>;

const initialState: ICommentsState = defaultState;

export const createComment = createAsyncThunk(
  'comment/create',
  async (payload: { text: string; postId: IPost['_id'] }) =>
    api.createComment(payload.text, payload.postId),
);

export const comments = createSlice({
  name: 'comments',
  initialState,
  reducers: defaultReducers<ICommentsState>(),
  extraReducers: builder => {
    builder.addCase(createComment.fulfilled, (state, { payload }) => {
      const { entities, result } = normalize(payload, commentSchema);
      state.byId[result] = entities.comments?.[result];
      state.all.unshift(result);
    });
  },
});

export const commentById = (state: RootState) => state.comments.byId;
export const allComments = (state: RootState) => state.comments.all;
