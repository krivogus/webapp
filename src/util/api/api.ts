import axios from 'axios';
import {
  authRequestInterceptor,
  authResponseInterceptor,
} from './interceptors';

export const api = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
});

api.interceptors.request.use(authRequestInterceptor);
api.interceptors.response.use(authResponseInterceptor);
