import moment from 'moment';

export const dateToMonthDayYear = (date: string) => {
  return moment(date).format('MMMM DD, YYYY');
};
