import React, { FC } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

interface IFriendListItem {
  username: string;
  avatarSrc: string;
}

const FriendListItem: FC<IFriendListItem> = ({ username, avatarSrc }) => (
  <ListItem>
    <ListItemAvatar>
      <Avatar alt={username} src={avatarSrc} />
    </ListItemAvatar>
    <ListItemText
      primaryTypographyProps={{ color: 'secondary', variant: 'body2' }}
      primary={username}
    />
  </ListItem>
);

export default FriendListItem;
