import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100vw',
      height: theme.spacing(8),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
  }),
);
