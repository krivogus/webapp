import React, { FC } from 'react';
import { useStyles } from './postItem.style';
import { IPost, IUser, ILikes, IComment } from '../../types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import Grid from '@material-ui/core/Grid';
import { getFullFileUrl } from '../../config';
import { dateToMonthDayYear } from '../../util/misc/date';
import Comment from './Comment';
import CommentInput from './CommentInput';
import clsx from 'clsx';

export interface IPostItemProps {
  me: Pick<IUser, 'profile' | '_id'>;
  post: IPost;
  addComment: (text: string, postId: string) => any;
  likePost: (postId: string) => any;
  unlikePost: (postId: string) => any;
  isLikedByMe: boolean;
}

const PostItem: FC<IPostItemProps> = ({
  me,
  post,
  addComment,
  likePost,
  unlikePost,
  isLikedByMe,
}) => {
  const classes = useStyles({});
  const [expanded, setExpanded] = React.useState(false);
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Grid className={classes.wrapper}>
      <Card classes={{ root: classes.root }}>
        <CardHeader
          avatar={
            <Avatar
              src={
                post.author.profile?.avatarSrc &&
                getFullFileUrl(post.author.profile.avatarSrc)
              }
              aria-label="recipe"
              className={classes.avatar}
            >
              R
            </Avatar>
          }
          action={
            <IconButton disabled aria-label="settings">
              <MoreVertIcon />
            </IconButton>
          }
          title={post.author.username}
          subheader={dateToMonthDayYear(post.createdAt as string)}
        />
        {post.data.photo && (
          <CardMedia
            className={classes.media}
            image={getFullFileUrl(post.data.photo)}
            title="Paella dish"
          />
        )}
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            {post.data.text}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <Grid container wrap="nowrap" justify="space-between">
            <Grid container item>
              <IconButton
                className={clsx({
                  [classes.like]: true,
                  [classes.liked]: isLikedByMe,
                })}
                onClick={
                  isLikedByMe
                    ? () => unlikePost(post._id as string)
                    : () => likePost(post._id as string)
                }
                aria-label="add to favorites"
              >
                <FavoriteIcon />
              </IconButton>
              <IconButton disabled aria-label="share">
                <ShareIcon />
              </IconButton>
            </Grid>
            <Grid item container justify="flex-end">
              <IconButton disabled>
                <Typography
                  variant="body2"
                  className={classes.statisticsTypography}
                >
                  {(post.likes as ILikes)?.amount || 0}
                </Typography>
                <ThumbUpIcon />
              </IconButton>
              <IconButton disabled>
                <Typography
                  variant="body2"
                  className={classes.statisticsTypography}
                >
                  {post.comments?.length || 0}
                </Typography>
                <ChatBubbleIcon />
              </IconButton>
              <IconButton
                className={clsx(classes.expand, {
                  [classes.expandOpen]: expanded,
                  [classes.statisticsTypography]: true,
                })}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
              >
                <ExpandMoreIcon />
              </IconButton>
            </Grid>
          </Grid>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <>
              {(post.comments as IComment[])?.map(comment => (
                <Comment {...comment} key={comment._id as string} />
              ))}
              <CommentInput
                profile={me.profile}
                addComment={text => addComment(text, post._id as string)}
              />
            </>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  );
};

export default PostItem;
