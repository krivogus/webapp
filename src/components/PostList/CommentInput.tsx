import React, { FC, MouseEvent, useState, FormEvent, ChangeEvent } from 'react';
import { useStyles } from './commentInput.style';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import { getFullFileUrl } from '../../config';
import { InputBase } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import SendOutlinedIcon from '@material-ui/icons/SendOutlined';
import { IUser } from 'src/types';

export interface ICommentInputProps {
  profile: IUser['profile'];
  addComment: (text: string) => any;
}

const CommentInput: FC<ICommentInputProps> = ({ profile, addComment }) => {
  const classes = useStyles({});
  const [text, setText] = useState('');
  const onSubmit = async (
    e: MouseEvent<HTMLButtonElement> | FormEvent<HTMLFormElement>,
  ) => {
    e.preventDefault();
    await addComment(text);
    setText('');
  };
  const canBeSent = !!text;
  const onTextChange = (
    e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => {
    setText(e.currentTarget.value);
  };
  return (
    <form onSubmit={onSubmit}>
      <Grid
        container
        direction="row"
        wrap="nowrap"
        className={classes.root}
        spacing={2}
      >
        <Grid item>
          <Avatar
            src={profile?.avatarSrc && getFullFileUrl(profile.avatarSrc)}
            aria-label="recipe"
          >
            R
          </Avatar>
        </Grid>
        <Grid xs item container alignItems="center" spacing={1}>
          <Grid item xs>
            <InputBase
              onChange={onTextChange}
              placeholder="Your comment..."
              fullWidth
              value={text}
            />
          </Grid>
          <Grid item>
            <IconButton
              aria-label="send"
              onClick={onSubmit}
              disabled={!canBeSent}
            >
              {canBeSent ? <SendIcon /> : <SendOutlinedIcon />}
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

export default CommentInput;
