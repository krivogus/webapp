import { schema } from 'normalizr';

export const userSchema = new schema.Entity('users', undefined, {
  idAttribute: '_id',
});
export const postSchema = new schema.Entity('posts', undefined, {
  idAttribute: '_id',
});
export const likesSchema = new schema.Entity('likes', undefined, {
  idAttribute: '_id',
});
export const commentSchema = new schema.Entity('comments', undefined, {
  idAttribute: '_id',
});

likesSchema.define({ users: [userSchema], post: postSchema });
commentSchema.define({ post: postSchema, author: userSchema });
postSchema.define({
  likes: likesSchema,
  comments: [commentSchema],
  author: userSchema,
});
userSchema.define({
  friends: [userSchema],
  posts: [postSchema],
  likes: likesSchema,
});
