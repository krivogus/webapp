import React, { FC } from 'react';
import { IUser } from 'src/types';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { getFullFileUrl } from '../../config';
import { useStyles } from './index.style';

export interface IFriendListItemProps {
  friend: IUser;
}

const FriendListItem: FC<IFriendListItemProps> = ({ friend }) => {
  const classes = useStyles();
  const avatarSrc = friend.profile?.avatarSrc;

  return (
    <Grid
      container
      direction="row"
      wrap="nowrap"
      className={classes.friendListRoot}
    >
      <Grid item container className={classes.imageWrapper}>
        {avatarSrc ? (
          <Paper className={classes.imageContainer}>
            <img
              src={getFullFileUrl(avatarSrc)}
              alt="profile"
              className={classes.avatarImage}
            />
          </Paper>
        ) : (
          <Paper className={classes.imageContainer}>asd</Paper>
        )}
      </Grid>
      <Grid xs item container direction="column" spacing={1}>
        <Grid item container direction="column" spacing={1}>
          <Grid item>
            <Typography variant="h6">{friend.username}</Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default FriendListItem;
