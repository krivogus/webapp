import React, { FC } from 'react';
import { useStyles } from './postItem.style';
import { IComment } from '../../types';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { getFullFileUrl } from '../../config';
import { dateToMonthDayYear } from '../../util/misc/date';

interface ICommentProps extends IComment {}

const Comment: FC<ICommentProps> = ({ data, author, createdAt }) => {
  const classes = useStyles({});

  return (
    <Grid container direction="row" wrap="nowrap" spacing={2}>
      <Grid item>
        <Avatar
          src={
            author.profile?.avatarSrc &&
            getFullFileUrl(author.profile.avatarSrc)
          }
          aria-label="recipe"
          className={classes.avatar}
        >
          R
        </Avatar>
      </Grid>
      <Grid xs item container direction="column" spacing={1}>
        <Grid item container direction="column" spacing={1}>
          <Grid item>
            <Typography variant="body1">{author.username}</Typography>
          </Grid>
          <Grid item>
            <Typography variant="body2">{data.text}</Typography>
          </Grid>
        </Grid>
        <Grid item container>
          <Typography color="textSecondary" variant="caption">
            {dateToMonthDayYear(createdAt as string)}
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Comment;
