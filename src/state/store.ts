import {
  configureStore,
  getDefaultMiddleware,
  combineReducers,
  Action,
} from '@reduxjs/toolkit';
import { createLogger } from 'redux-logger';
import * as ducks from './ducks';
import { ThunkAction } from 'redux-thunk';

export const reducer = combineReducers({
  users: ducks.users.reducer,
  auth: ducks.auth.reducer,
  posts: ducks.posts.reducer,
  likes: ducks.likes.reducer,
  comments: ducks.comments.reducer,
});

export type RootState = ReturnType<typeof reducer>;
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;

export const store = configureStore({
  middleware: [createLogger(), ...getDefaultMiddleware()] as const,
  reducer,
});

export type AppDispatch = typeof store.dispatch;
