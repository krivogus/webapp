import { Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    item: {
      margin: theme.spacing(2),
    },
    layoutRoot: {
      display: 'grid',
      [theme.breakpoints.up('md')]: {
        gridTemplate: 'auto / minmax(300px, 340px) auto 280px',
      },
      [theme.breakpoints.between('sm', 'md')]: {
        gridTemplate: 'auto 1fr / minmax(280px, 340px) minmax(300px, auto)',
        gridTemplateAreas: '"top main_content" "bottom main_content"',
      },
      [theme.breakpoints.down('xs')]: {
        gridTemplateColumns: 'minmax(280px, auto)',
      },
    },
    profile: {
      [theme.breakpoints.between('sm', 'md')]: {
        gridArea: 'top',
      },
    },
    friends: {
      [theme.breakpoints.between('sm', 'md')]: {
        gridArea: 'bottom',
      },
    },
    posts: {
      [theme.breakpoints.between('sm', 'md')]: {
        gridArea: 'main_content',
      },
      [theme.breakpoints.down('sm')]: {
        order: 1,
      },
    },
  }),
);
