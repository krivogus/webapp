import React, { FC, MouseEvent, useRef } from 'react';
import Header from '../../components/Header';
import * as state from 'src/state';
import { useDispatch, useSelector } from 'react-redux';
import { Grid } from '@material-ui/core';
import { useStyles } from './index.style';
import ProfileCard from 'src/components/ProfileCard';
import clsx from 'clsx';
import { getFullFileUrl, config } from 'src/config';

const Layout: FC = ({ children }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state.currentUser);

  const onLogout = () => dispatch(state.logout());
  const classes = useStyles();
  const fileInputRef = useRef<HTMLInputElement | null>(null);

  const onUploadAvatar = (e: MouseEvent<HTMLLabelElement>) => {
    const files = fileInputRef.current?.files;
    if (!files) return;
    dispatch(state.uploadAvatar(files[0]));
  };

  return currentUser ? (
    <>
      <Header onLogout={onLogout} links={config.router} />
      <Grid container spacing={2} className={classes.layoutRoot}>
        <Grid className={clsx(classes.item, classes.profile)}>
          <ProfileCard
            uploadAvatar={onUploadAvatar}
            ref={fileInputRef}
            username={currentUser.username}
            avatarSrc={
              currentUser.profile?.avatarSrc &&
              getFullFileUrl(currentUser.profile.avatarSrc)
            }
          />
        </Grid>
        <Grid className={clsx(classes.item, classes.posts)}>{children}</Grid>
        <Grid className={clsx(classes.item, classes.friends)}>
          {/* <FriendListSidebar friends={user.friends} /> */}
        </Grid>
      </Grid>
    </>
  ) : null;
};

export default Layout;
