import React, { FC } from 'react';
import Layout from 'src/containers/Layout';
import PostList from 'src/components/PostList';
import { useSelector, useDispatch } from 'react-redux';
import PostInput from 'src/containers/PostInput';
import * as state from 'src/state';

const Home: FC = () => {
  const currentUser = useSelector(state.currentUser);
  const allPosts = useSelector(state.allPosts);
  const dispatch = useDispatch();
  const likePost = (postId: string) => dispatch(state.likePost({ postId }));
  const unlikePost = (postId: string) => dispatch(state.unlikePost({ postId }));
  const createComment = (text: string, postId: string) =>
    dispatch(state.createComment({ text, postId }));

  return (
    <Layout>
      <PostInput />
      <PostList
        posts={allPosts}
        me={currentUser}
        addComment={createComment}
        likePost={likePost}
        unlikePost={unlikePost}
      />
    </Layout>
  );
};

export default Home;
