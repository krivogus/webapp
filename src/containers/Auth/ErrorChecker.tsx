import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Grid } from '@material-ui/core';
import { FC } from 'react';

interface IErrorCheckerProps {
  text: string;
  isChecked: boolean;
}

const ErrorChecker: FC<IErrorCheckerProps> = ({ text, isChecked }) => (
  <Grid item>
    <FormControlLabel
      control={
        <Checkbox
          value={text}
          color="primary"
          size="small"
          disableFocusRipple
          disableRipple
        />
      }
      label={text}
      checked={isChecked}
    />
  </Grid>
);

export default ErrorChecker;
