import { IUser } from './user';
import { IPost } from './post';
import { DBProps } from './misc';

export interface ILikes extends DBProps {
  users: IUser[] | string[];
  amount: number;
  post: IPost;
}
