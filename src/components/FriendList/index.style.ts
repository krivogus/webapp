import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

const photoSize = 14;

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      padding: theme.spacing(4),
    },
    avatarImage: {
      minWidth: '100%',
      height: '100%',
    },
    imageContainer: {
      width: '100%',
      height: '100%',
      overflow: 'hidden',
    },
    imageWrapper: {
      width: theme.spacing(photoSize),
      height: theme.spacing(photoSize),
    },
    friendListRoot: {
      '& > *': {
        margin: theme.spacing(2),
      },
    },
  }),
);
