import { Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      marginTop: theme.spacing(3),
      height: theme.spacing(5),
    },
    positionAbsolute: {
      position: 'absolute',
    },
  }),
);
