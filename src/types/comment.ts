import { IUser } from './user';
import { IPost } from './post';
import { DBProps } from './misc';

export interface IComment extends DBProps {
  post: IPost;
  createdAt: string;
  author: IUser;
  data: {
    text: string;
  };
}
