import React, { FC, MouseEvent } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { useStyles } from './index.style';
import Logo from '../Logo';
import { Grid } from '@material-ui/core';
import HouseIcon from '@material-ui/icons/House';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import { Link } from 'react-router-dom';
interface IHeaderProps {
  onLogout: (event: MouseEvent<HTMLButtonElement>) => void;
  links: {
    friends: string;
    home: string;
  };
}

const Header: FC<IHeaderProps> = ({ onLogout, links }) => {
  const classes = useStyles({});

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <Grid container justify="space-between" direction="row">
            <Logo />
            <Grid item wrap="nowrap">
              <Link to={links.home}>
                <IconButton>
                  <HouseIcon color="secondary" />
                </IconButton>
              </Link>
              <Link to={links.friends}>
                <IconButton>
                  <PeopleAltIcon color="secondary" />
                </IconButton>
              </Link>
              <Button color="inherit" onClick={onLogout}>
                Logout
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
