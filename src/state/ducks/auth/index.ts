import { createSlice, Dispatch } from '@reduxjs/toolkit';
import { AppThunk, RootState } from 'src/state/store';
import { StateT, IUser, defaultState } from 'src/types';
import { removeAuthToken } from 'src/util/storage';
import { userSchema } from 'src/state/normalizers';
import { normalize } from 'normalizr';
import { users as usersSlice } from 'src/state/ducks/users';
import { defaultReducers } from '../util';
import * as state from 'src/state';
import * as api from './api';

export type IAuthState = Pick<
  StateT<IUser>,
  'currentId' | 'error' | 'isFetching'
>;

const initialState: IAuthState = {
  currentId: defaultState.currentId,
  error: defaultState.error,
  isFetching: defaultState.isFetching,
};

export const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    ...defaultReducers<IAuthState>(),
    clear: state => {
      state.currentId = null;
      state.error = null;
      state.isFetching = false;
    },
  },
});

const { request, success, error, clear } = auth.actions;

export const userSuccess = (user: IUser) => async (dispatch: Dispatch) => {
  const { entities, result } = normalize(user, userSchema);
  dispatch(success({ currentId: result }));
  dispatch(usersSlice.actions.success({ byId: entities.users }));
  dispatch(state.posts.actions.success({ byId: entities.posts }));
  dispatch(state.likes.actions.success({ byId: entities.likes }));
  dispatch(state.comments.actions.success({ byId: entities.comments }));
};

export const authenticate = (
  username: string,
  password: string,
): AppThunk => async dispatch => {
  try {
    dispatch(request());
    const user = await api.login(username, password);
    dispatch(userSuccess(user));
  } catch (err) {
    dispatch(error(err?.response?.data?.message || err.message));
  }
};

export const logout = (): AppThunk => dispatch => {
  dispatch(clear());
  removeAuthToken();
};

export const register = (
  username: string,
  password: string,
): AppThunk => async dispatch => {
  try {
    dispatch(request());
    const user = await api.register(username, password);
    dispatch(userSuccess(user));
  } catch (err) {
    dispatch(error(err?.response?.data?.message ?? err.message));
  }
};

export const getMe = (): AppThunk => async dispatch => {
  try {
    dispatch(request());
    const user = await api.getUser();
    dispatch(userSuccess(user));
  } catch {
    dispatch(clear());
  }
};

export const authUserId = (state: RootState) => state.auth.currentId;
